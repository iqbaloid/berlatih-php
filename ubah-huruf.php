<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>
<body>
    <h1>Berlatih Ubah Huruf</h1>
    <?php
        function ubah_huruf($string){
            $toString = [0=>'a', 1=>'b', 2=>'c', 3=>'d', 4=>'e', 5=>'f', 6=>'g', 7=>'h', 8=>'i', 9=>'j', 10=>'k', 11=>'l', 12=>'m', 13=>'n', 14=>'o', 15=>'p', 16=>'q', 17=>'r', 18=>'s', 19=>'t', 20=>'u', 21=>'v', 22=>'w', 23=>'x', 24=>'y', 25=>'z'];
            $toNum = ['a'=>0, 'b'=>1, 'c'=>2, 'd'=>3, 'e'=>4, 'f'=>5, 'g'=>6, 'h'=>7, 'i'=>8, 'j'=>9, 'k'=>10, 'l'=>11, 'm'=>12, 'n'=>13,  'o'=>14, 'p'=>15, 'q'=>16, 'r'=>17, 's'=>18, 't'=>19, 'u'=>20, 'v'=>21, 'w'=>22, 'x'=>23, 'y'=>24, 'z'=>25];
            
            $temp = strtolower($string);
            $string_data = str_split($temp);
            $new_data = [];

            foreach ($string_data as $value) {
                $new_data[] .= $toNum[$value] + 1 ."";
            }

            $num_data = $new_data;
            $result = "";
            foreach ($num_data as $value) {
                $result .= $toString[$value]."";
            }

            return $result. "<br>";
        }

        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu
    ?>
</body>
</html>